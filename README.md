**How to use it**

In the pom.xml specify the requested lib and the repository URL

```
#!xml

</dependencies>
    <dependency>
        <groupId>com.mpayme.testkit</groupId>
        <artifactId>ApiTestPom</artifactId>
        <version>2.2.0-SNAPSHOT</version>
    </dependency>
</dependencies>
 
<repositories>
    <repository>
        <id>maven-repo</id>
        <url>https://bitbucket.org/Denees/maven-repo/raw/master/repository/</url>
    </repository>
</repositories>
```